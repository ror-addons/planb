PlanB = {}
PlanB.data = {}
PlanB.data.page = {}
PlanB.careerOK = false

-------- GENERAL UTILITY FUNCTIONS --------

local function print(text)
    EA_ChatWindow.Print(StringToWString(text))
end

local function print_in_color(text, color)
	local wtext = StringToWString(text)

	-- 2 is actually the TELL_SEND filter (blue), 3 is an undefined(?) filter (happens to be yellow), and 4 is the GROUP filter (green)
	-- 0 is the SAY filter (white), and 1 is the TELL_RECEIVE filter (blue), but we don't deal with those...
	-- everything else we will treat as a combat text filter/color
	if (not color) then color = 3 end
	
	if (color > 4) then
		color = SystemData.ChatLogFilters.COMBAT_DEFAULT
		TextLogAddEntry("Combat", color, wtext)
	else
		TextLogAddEntry("Chat", color, wtext)
	end
end

local function display_alert(text)
	SystemData.AlertText.VecType = {SystemData.AlertText.Types.ABILITY}
	SystemData.AlertText.VecText = {StringToWString(text)}
	AlertTextWindow.AddAlert()
end


function PlanB.Initialize()	
	-- LibSlash check, a required dependency
	if (LibSlash == nil) then
		print_in_color("Warning: PlanB couldn't find LibSlash!")
		return
	end
	
	-- check to see if slash command "/planb" is available for planb
	if LibSlash.IsSlashCmdRegistered("planb") then
		print_in_color("Warning: something else seems to be using /planb - no room for PlanB.")
		return
	end     
	
	-- register slash command "/planb"
	LibSlash.RegisterSlashCmd("planb", function(input) PlanB.Command(input) end)

	-- Turn the mod on if not saved from last session
	if ( PlanB.data.active == nil ) then
		PlanB.data.active = true
	end

	-- set the default hotbar as 1 if not saved from last session
	if ( PlanB.data.hotbar == nil ) then
		PlanB.data.hotbar = 1
	end

	-- set the default pages to 1,2,3 if not saved from last session
	if ( PlanB.data.page[0] == nil ) then
		PlanB.data.page[0] = 1
	end
	if ( PlanB.data.page[1] == nil ) then
		PlanB.data.page[1] = 2
	end
	if ( PlanB.data.page[2] == nil ) then
		PlanB.data.page[2] = 3
	end

	-- Set chat spam flag if not saved from last session
	if ( PlanB.data.silent == nil ) then
		PlanB.data.silent = false
	end

	-- Set page max if not saved from last session
	if ( PlanB.data.pagemax == nil ) then
		PlanB.data.pagemax = 5
	end

	-- register our function for setting the correct page to be called whenever these important events are triggered
	-- this is when plan/balance changes normally (ie. you use an action that leads to the next state)
	RegisterEventHandler(SystemData.Events.PLAYER_CAREER_RESOURCE_UPDATED, "PlanB.SetPage")
	-- when you die
	RegisterEventHandler(SystemData.Events.PLAYER_DEATH, "PlanB.SetPage")
	-- when you load into a new area
	RegisterEventHandler(SystemData.Events.LOADING_END, "PlanB.HandleLoading")
	-- when you enter (and leave?) combat
	RegisterEventHandler(SystemData.Events.PLAYER_COMBAT_FLAG_UPDATED, "PlanB.SetPage")
	-- when you reload the UI
	RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "PlanB.HandleLoading")
end

-- Parses user input to set-up/active planb
function PlanB.Command(input)
	--if no input, toggle active and make sure page is set correctly and return
	-- %S matches any non-whitespace character, so if we don't find a match, then there's nothing but whitespace
	if ( string.find(input, "%S") == nil ) then
		PlanB.data.active = not PlanB.data.active
		PlanB.HandleLoading()
		if ( PlanB.data.silent == false) then PlanB.DisplaySettings() end
		return
	end
	
	-- convert to lower-case
	input = string.lower(input)
	-- condense multiple spaces into one
	input = string.gsub(input, "(%s+)", " ")
	-- remove spaces around '=' and ','
	input = string.gsub(input, "%s*([=,])%s*", "%1")
		
	-- loop over the strings in the parameter list
	local gmatch = function( str, pattern )
		local init = 1
		local function gmatch_it()
			if init <= str:len() then
				local s, e = str:find(pattern, init)
				if s then
					local res = {str:match(pattern, init)}
					init = e+1
					return unpack(res)
				end
			end
		end
		return gmatch_it
	end
	for substring in gmatch(input, "([%w%p=]+)") do
		-- grab out the command and any possible value associated
		command = string.match(substring, "(%w+)=?")
		value = string.match(substring, "=([%d,]+)")

		print(command)
		-- check what the command is and do something appropriate
		if ( command == "usage" or command == "help" ) then
			PlanB.Usage()
			return
		elseif ( command == "settings" ) then
			PlanB.DisplaySettings()
			return
		elseif ( command == "pagemax" and value ~= nil ) then
			local newmax = tonumber(value)
			if ( newmax < 5 or newmax > 9 ) then
				print_in_color("Warning - " .. value .. " is not a valid hotbar page maximum (must be 5-9)", 4)
			else
				PlanB.data.pagemax = newmax
				PlanB.SetPageMax()
				print_in_color("Changed allowed page count to " .. GameData.HOTBAR_SWAPPABLE_PAGE_COUNT, 4)
			end
			return
		elseif ( command == "silent" ) then
			PlanB.data.silent = not PlanB.data.silent
			if ( PlanB.data.silent ) then
				print_in_color("PlanB is now silent")
			else
				print_in_color("PlanB is no longer silent")
			end
			return
		elseif ( command == "hotbar" and value ~= nil ) then
			--set the target hotbar
			local newbar = tonumber(value)
			if ( newbar < 1 or newbar > 4 ) then
				print_in_color("Warning - " .. value .. " is not a valid hotbar number (must be 1-4)", 4)
			else
				PlanB.data.hotbar = newbar
			end

		elseif ( command == "pages" and value ~= nil ) then
			--grab out the specified page numbers
			itr = gmatch(value, "([%d]+),?")
			for i=0,2,1 do
				local thisPage = tonumber(itr())
				if ( thisPage < 1 or thisPage > 9 ) then
					print_in_color("Warning - " .. tostring(thisPage) .. " is not a valid hotbar page (must be 1-9)", 4)
				else
					PlanB.data.page[i] = thisPage
				end
			end
		end
	end

	-- indicate that planb is active and set the correct page number
	PlanB.SetPage()
	PlanB.DisplaySettings()
end

-- Checks if the user is a swordmaster / black orc
function PlanB.ValidCareer()
    -- Return true if the player's current career is compatible with PlanB, false otherwise
    local myCareer = GameData.Player.career.line
    if ( myCareer == GameData.CareerLine.SWORDMASTER or myCareer == GameData.CareerLine.BLACK_ORC )
	then
        return true
    else
        return false
    end
end

-- Sets the target hotbars page based on the current balance/plan
function PlanB.SetPage()
	-- if planb is inactive, do nothing
	if ( PlanB.data.active == false ) then return end
	
	-- Only change hotbar page if the career is OK
	if ( PlanB.careerOK == true ) then
		-- set the target actionbar to the appropriate page
		SetHotbarPage(PlanB.data.hotbar, PlanB.data.page[CareerResource:GetCurrent()])
	end
end

-- Checks after loading if the career is OK
function PlanB.HandleLoading()
	PlanB.careerOK = false
	if ( PlanB.ValidCareer() == true ) then
		PlanB.careerOK = true
		PlanB.SetPageMax()
	end
	
	-- set the target actionbar to the appropriate page
	PlanB.SetPage()
end


-- Prints out some info about current settings
function PlanB.DisplaySettings()
	if ( PlanB.data.active == true ) then
		print_in_color("PlanB activated.", 4)
		if ( PlanB.careerOK == true ) then
			print_in_color("Using hotbar " .. tostring(PlanB.data.hotbar) .. ", cycling pages " .. tostring(PlanB.data.page[0]) .. " -> " .. tostring(PlanB.data.page[1]) .. " -> " .. tostring (PlanB.data.page[2]), 4)
			print_in_color("Showing " .. GameData.HOTBAR_SWAPPABLE_PAGE_COUNT .. " pages on each hotbar", 4)
		else
			print_in_color("PlanB does not work with the " .. tostring(GameData.Player.career.name) .. " career.")	
		end
	else
		print_in_color("PlanB deactived.")
	end
end

function PlanB.Usage()
	print_in_color("Planned Balance (aka PlanB)")
	print_in_color("/planb [hotbar=#] [pages=#,#,#]")
	print_in_color("Defaults: hotbar=1 pages=1,2,3")
	print_in_color("If no parameters are passed, PlanB is simply enabled/disabled.")
	print_in_color("ex1: /planb pages=1,2,3", 4)
	print_in_color("Rotates through pages 1,2,3 on hotbar 1.")
	print_in_color("ex2: /planb pages=1,1,2 hotbar=2", 4)
	print_in_color("Rotates through pages 1,1,2 on hotbar 2.")
	print_in_color("Note that the same page can be used for multiple states.")
	print_in_color("/planb settings - Tells you the current hotbar and pages used")
	print_in_color("/planb silent - Toggles on/off chat spam (for /planb macros)")
	print_in_color("/planb pagemax=# - Sets maximum number of pages shown in hotbars")
end

function PlanB.SetPageMax()
	-- Switch maximum allowed pages in hotbars
	GameData.HOTBAR_SWAPPABLE_PAGE_COUNT = PlanB.data.pagemax
end